package com.learn.kafka.courier.listener;

import com.learn.kafka.entity.dto.OrderDTO;
import com.learn.kafka.entity.enumaration.OrderStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class NotificationTopicListenerTest {

    @InjectMocks
    private NotificationTopicListener notificationTopicListener;

    @Mock
    private KafkaTemplate<Long, OrderDTO> kafkaTemplateMock;

    @Mock
    private OrderDTO orderDTOMock;

    @Test
    public void processIncomeOrderTest() throws InterruptedException {
        String notificationTopicName = "notification";
        int clientPartition = 0;
        long orderId = 1L;

        notificationTopicListener.processIncomeOrder(orderDTOMock, orderId);

        verify(orderDTOMock).setOrderStatus(OrderStatus.DELIVERING);
        verify(orderDTOMock).setOrderStatus(OrderStatus.DELIVERED);
        verify(kafkaTemplateMock, times(2))
                .send(notificationTopicName, clientPartition, orderId, orderDTOMock);
    }
}