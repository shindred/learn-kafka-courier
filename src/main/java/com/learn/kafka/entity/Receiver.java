package com.learn.kafka.entity;

import lombok.Data;

@Data
public class Receiver {

    private Long id;
    private String firstName;
    private String secondName;
    private String address;
}
