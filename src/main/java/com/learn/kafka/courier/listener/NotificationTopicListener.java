package com.learn.kafka.courier.listener;

import com.learn.kafka.entity.dto.OrderDTO;
import com.learn.kafka.entity.enumaration.OrderStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class NotificationTopicListener {

    private static final int CLIENT_PARTITION = 0;
    private static final String NOTIFICATION_TOPIC_NAME = "notification";

    private final KafkaTemplate<Long, OrderDTO> orderDTOKafkaTemplate;

    @KafkaListener(topicPartitions = @TopicPartition(topic = NOTIFICATION_TOPIC_NAME, partitions = {"1"}))
    public void processIncomeOrder(@Payload OrderDTO orderDTO, @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) Long id) throws InterruptedException {
        Thread.sleep(7000);
        sendNotification(OrderStatus.DELIVERING, orderDTO, id);
        Thread.sleep(10000);
        sendNotification(OrderStatus.DELIVERED, orderDTO, id);
    }

    private void sendNotification(OrderStatus orderStatus, OrderDTO orderDTO, Long id) {
        orderDTO.setOrderStatus(orderStatus);

        orderDTOKafkaTemplate.send(NOTIFICATION_TOPIC_NAME, CLIENT_PARTITION, id, orderDTO);
    }
}
